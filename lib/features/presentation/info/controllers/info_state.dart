import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/features/domain/entities/comment.dart';

part 'info_state.freezed.dart';

@freezed
class InfoState with _$InfoState {
  const factory InfoState.loading() = Loading;
  const factory InfoState.success(List<Comment> comments) = Success;
  const factory InfoState.error(Failure failure) = Error;
}
