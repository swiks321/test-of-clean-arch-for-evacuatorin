import 'package:bloc/bloc.dart';
import 'package:test_work/features/domain/usecases/get_comments.dart';

import 'info_state.dart';

class InfoCubit extends Cubit<InfoState> {
  InfoCubit(this.getComments) : super(const Loading());

  final GetComments getComments;

  Future<void> init(int postId) async {
    emit(Loading());

    final commentsOrFailure = await getComments(postId);
    commentsOrFailure.fold(
      (failure) => emit(Error(failure)),
      (comments) => emit(Success(comments)),
    );
  }
}
