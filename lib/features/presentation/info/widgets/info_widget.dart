import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_work/core/loc/localization.dart';
import 'package:test_work/features/domain/entities/comment.dart';
import 'package:test_work/features/domain/entities/post.dart';

class InfoWidget extends StatelessWidget {
  const InfoWidget({Key? key, required this.post, this.comments})
      : super(key: key);

  final Post post;
  final List<Comment>? comments;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: Get.height,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Card(
              margin: EdgeInsets.zero,
              elevation: 10,
              child: Container(
                width: Get.width,
                padding: const EdgeInsets.symmetric(horizontal: 18.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 16.0),
                    Placeholder(fallbackHeight: 200),
                    const SizedBox(height: 16.0),
                    Hero(
                      tag: post,
                      child: Text(
                        post.title,
                        style: context.theme.textTheme.headline6!
                            .copyWith(color: context.theme.primaryColorDark),
                      ),
                    ),
                    const SizedBox(height: 16.0),
                    Text(
                      post.body,
                      style: context.theme.textTheme.bodyText1,
                    ),
                    const SizedBox(height: 32.0),
                  ],
                ),
              ),
            ),
            if (comments == null)
              Container(
                margin: const EdgeInsets.only(top: 24),
                alignment: Alignment.center,
                child: const CircularProgressIndicator(),
              ),
            if (comments != null)
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(left: 20, bottom: 8, top: 24),
                child: Text(
                  Locals.lastLocale.comments,
                  style: context.theme.textTheme.headline6!
                      .copyWith(color: context.theme.primaryColorDark),
                ),
              ),
            if (comments != null) _buildComments(context, comments!),
          ],
        ),
      ),
    );
  }

  Widget _buildComments(BuildContext context, List<Comment> comments) {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: comments.length,
      itemBuilder: (context, index) => ListTile(
        contentPadding: const EdgeInsets.all(16),
        title: Text(
          comments[index].name,
          style: context.textTheme.bodyText1,
        ),
        subtitle: Text(
          comments[index].body,
          style: context.textTheme.bodyText2,
        ),
        leading: Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: context.theme.primaryColorLight,
          ),
          child: Icon(
            Icons.flutter_dash,
            color: context.theme.primaryColorDark,
            size: 36,
          ),
        ),
      ),
      separatorBuilder: (context, index) => Divider(
        thickness: 10,
        color: context.theme.primaryColorDark.withOpacity(0.2),
      ),
    );
  }
}
