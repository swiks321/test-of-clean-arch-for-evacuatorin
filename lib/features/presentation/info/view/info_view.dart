import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:test_work/core/di/injector.dart';
import 'package:test_work/core/widgets/index.dart' as core_widgets;
import 'package:test_work/features/domain/entities/post.dart';
import 'package:test_work/features/presentation/info/controllers/info_cubit.dart';
import 'package:test_work/features/presentation/info/controllers/info_state.dart';
import 'package:test_work/features/presentation/info/widgets/info_widget.dart';

class InfoView extends StatelessWidget {
  const InfoView({Key? key, required this.post}) : super(key: key);

  final Post post;

  Widget _buildBody(BuildContext context) {
    return BlocBuilder<InfoCubit, InfoState>(builder: (context, state) {
      return state.when(
        loading: () => InfoWidget(post: post),
        success: (comments) => InfoWidget(post: post, comments: comments),
        error: (failure) => core_widgets.ErrorWidget(failure),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: context.theme.scaffoldBackgroundColor,
      body: SafeArea(
        child: BlocProvider(
          create: (context) => Injector.resolve<InfoCubit>()..init(post.id),
          child: _buildBody(context),
        ),
      ),
    );
  }
}
