import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/core/loc/localization.dart';
import 'package:test_work/core/routes/pages.dart';
import 'package:test_work/features/domain/usecases/login.dart';

import 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit(this.loginUser) : super(const Initial());

  final Login loginUser;

  Future<void> onLogin(String login, String password) async {
    emit(const Loading());
    final Either<Failure, bool> loginOrFailure =
        await loginUser(LoginParams(login: login, password: password));
    loginOrFailure.fold(
      (failure) => emit(Error(failure)),
      (login) {
        if (login) {
          Get.offAndToNamed(Pages.home);
        } else {
          emit(const Initial());
          Get.snackbar(
            Locals.lastLocale.invalidData,
            Locals.lastLocale.userNotFound,
            icon: Icon(Icons.error),
          );
        }
      },
    );
  }
}
