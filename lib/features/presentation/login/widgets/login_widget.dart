import 'package:flutter/material.dart';
import 'package:test_work/core/loc/localization.dart';
import 'package:test_work/core/utils/text_field_validators.dart';
import 'package:test_work/core/widgets/index.dart';
import 'package:test_work/core/widgets/primary_button_widget.dart';

class LoginWidget extends StatelessWidget {
  const LoginWidget({
    Key? key,
    required this.onLogin,
  }) : super(key: key);

  final Function(String, String) onLogin;

  @override
  Widget build(BuildContext context) {
    final TextEditingController loginController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();

    final _formKey = GlobalKey<FormState>();

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 70),
      child: Form(
        key: _formKey,
        child: Column(children: [
          const Spacer(),
          TextFieldWidget(
            validator: TextFieldValidators.notEmptyValidator,
            label: Locals.lastLocale.login,
            controller: loginController,
          ),
          const SizedBox(height: 30),
          TextFieldWidget(
            validator: TextFieldValidators.notEmptyValidator,
            label: Locals.lastLocale.password,
            controller: passwordController,
          ),
          const Spacer(),
          PrimaryButtonWidget(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                onLogin(loginController.text, passwordController.text);
              }
            },
            text: Locals.lastLocale.signIn,
          ),
          const SizedBox(height: 80),
        ]),
      ),
    );
  }
}
