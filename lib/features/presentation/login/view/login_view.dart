import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:test_work/core/di/injector.dart';
import 'package:test_work/core/widgets/index.dart' as core_widgets;
import 'package:test_work/features/presentation/login/controllers/login_cubit.dart';
import 'package:test_work/features/presentation/login/controllers/login_state.dart';
import 'package:test_work/features/presentation/login/widgets/login_widget.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  Widget _buildBody(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(builder: (context, state) {
      return state.when(
        initial: () => LoginWidget(
          onLogin: BlocProvider.of<LoginCubit>(context).onLogin,
        ),
        loading: () => const core_widgets.LoadingWidget(),
        success: () => Container(),
        error: (failure) => core_widgets.ErrorWidget(failure),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: context.theme.scaffoldBackgroundColor,
      body: BlocProvider(
        create: (context) => Injector.resolve<LoginCubit>(),
        child: _buildBody(context),
      ),
    );
  }
}
