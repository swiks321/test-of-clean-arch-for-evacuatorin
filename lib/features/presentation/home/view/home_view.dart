import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:test_work/core/di/injector.dart';
import 'package:test_work/core/widgets/index.dart' as core_widgets;
import 'package:test_work/features/presentation/home/controllers/home_cubit.dart';
import 'package:test_work/features/presentation/home/controllers/home_state.dart';
import 'package:test_work/features/presentation/home/widgets/home_widget.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  Widget _buildBody(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(builder: (context, state) {
      return state.when(
        initial: () => const core_widgets.LoadingWidget(),
        loading: () => const core_widgets.LoadingWidget(),
        success: (facts) => HomeWidget(posts: facts),
        error: (failure) => core_widgets.ErrorWidget(failure),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: context.theme.scaffoldBackgroundColor,
      body: SafeArea(
        child: BlocProvider(
          create: (context) => Injector.resolve<HomeCubit>()..init(),
          child: _buildBody(context),
        ),
      ),
    );
  }
}
