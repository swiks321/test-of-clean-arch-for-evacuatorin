import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_work/core/loc/localization.dart';
import 'package:test_work/core/widgets/index.dart';
import 'package:test_work/features/domain/entities/post.dart';
import 'package:test_work/features/presentation/info/view/info_view.dart';

class HomeWidget extends StatelessWidget {
  const HomeWidget({Key? key, required this.posts}) : super(key: key);

  final List<Post> posts;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      shrinkWrap: true,
      itemCount: posts.length,
      itemBuilder: (context, index) => Card(
        margin: const EdgeInsets.all(10.0),
        elevation: 2.0,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: IntrinsicHeight(
            child: Row(
              children: [
                Flexible(
                  fit: FlexFit.loose,
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8.0,
                      vertical: 20.0,
                    ),
                    child: Placeholder(),
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 16.0),
                      Hero(
                        tag: posts[index],
                        child: Text(
                          posts[index].title,
                          style: context.theme.textTheme.headline6!
                              .copyWith(color: context.theme.primaryColorDark),
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      Text(
                        posts[index].body,
                        maxLines: 3,
                        overflow: TextOverflow.fade,
                        style: context.theme.textTheme.bodyText1,
                      ),
                      const SizedBox(height: 16.0),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: SecondaryButtonWidget(
                          text: Locals.lastLocale.more,
                          onPressed: () =>
                              Get.to(() => InfoView(post: posts[index])),
                        ),
                      ),
                      const SizedBox(height: 8.0),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
