import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/core/usecases/usecases.dart';
import 'package:test_work/features/domain/entities/post.dart';
import 'package:test_work/features/domain/usecases/get_posts.dart';

import 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit(this.getPosts) : super(const Initial());

  final GetPosts getPosts;

  Future<void> init() async {
    await loadPosts();
  }

  Future<void> loadPosts() async {
    emit(const Loading());
    final Either<Failure, List<Post>> postsOrFailure =
        await getPosts(NoParams());
    postsOrFailure.fold(
      (failure) => emit(Error(failure)),
      (posts) => emit(Success(posts)),
    );
  }
}
