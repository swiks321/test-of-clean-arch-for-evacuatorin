import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/features/domain/entities/post.dart';

part 'home_state.freezed.dart';

@freezed
class HomeState with _$HomeState {
  const factory HomeState.initial() = Initial;
  const factory HomeState.loading() = Loading;
  const factory HomeState.success(List<Post> posts) = Success;
  const factory HomeState.error(Failure failure) = Error;
}
