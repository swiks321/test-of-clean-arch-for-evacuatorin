import 'package:dartz/dartz.dart';

import 'package:test_work/core/error/failure.dart';

abstract class UsersRepository {
  Future<Either<Failure, bool>> checkUser({required String login, required String password});
}
