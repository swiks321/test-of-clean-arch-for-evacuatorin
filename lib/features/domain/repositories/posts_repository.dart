import 'package:dartz/dartz.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/features/domain/entities/comment.dart';
import 'package:test_work/features/domain/entities/post.dart';

abstract class PostsRepository {
  Future<Either<Failure, List<Post>>> getPosts();

  Future<Either<Failure, List<Comment>>> getComments(int postId);
}
