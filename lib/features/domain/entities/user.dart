import 'package:crypto/crypto.dart';
import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String login;
  final Digest passwordHash;

  User({required this.login, required this.passwordHash});

  @override
  List<Object?> get props => [login, passwordHash];
}
