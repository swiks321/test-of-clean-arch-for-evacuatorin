import 'package:equatable/equatable.dart';

class Comment extends Equatable {
  final int id;
  final int postId;
  final String name;
  final String body;

  Comment({
    required this.id,
    required this.postId,
    required this.name,
    required this.body,
  });

  @override
  List<Object?> get props => [
        id,
        postId,
        name,
        body,
      ];
}
