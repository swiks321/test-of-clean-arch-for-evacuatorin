import 'package:dartz/dartz.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/core/usecases/usecases.dart';
import 'package:test_work/features/domain/entities/post.dart';
import 'package:test_work/features/domain/repositories/posts_repository.dart';

class GetPosts extends UseCase<List<Post>, NoParams> {
  final PostsRepository _repository;

  GetPosts(this._repository);

  @override
  Future<Either<Failure, List<Post>>> call(NoParams params) {
    return _repository.getPosts();
  }
}
