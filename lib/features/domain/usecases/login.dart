import 'package:dartz/dartz.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/core/usecases/usecases.dart';
import 'package:test_work/features/domain/repositories/users_repository.dart';

class LoginParams {
  final String login;
  final String password;

  LoginParams({required this.login, required this.password});
}

class Login extends UseCase<bool, LoginParams> {
  final UsersRepository _repository;

  Login(this._repository);

  @override
  Future<Either<Failure, bool>> call(LoginParams params) {
    return _repository.checkUser(
      login: params.login,
      password: params.password,
    );
  }
}
