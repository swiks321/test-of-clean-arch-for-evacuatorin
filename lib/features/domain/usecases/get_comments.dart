import 'package:dartz/dartz.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/core/usecases/usecases.dart';
import 'package:test_work/features/domain/entities/comment.dart';
import 'package:test_work/features/domain/repositories/posts_repository.dart';

class GetComments extends UseCase<List<Comment>, int> {
  final PostsRepository _repository;

  GetComments(this._repository);

  @override
  Future<Either<Failure, List<Comment>>> call(int postId) {
    return _repository.getComments(postId);
  }
}
