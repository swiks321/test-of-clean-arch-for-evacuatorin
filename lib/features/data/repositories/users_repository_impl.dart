import 'package:dartz/dartz.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/features/data/datasources/datasource.dart';
import 'package:test_work/features/domain/repositories/users_repository.dart';

class UsersRepositoryImpl implements UsersRepository {
  final Datasource _datasource;

  UsersRepositoryImpl(this._datasource);

  @override
  Future<Either<Failure, bool>> checkUser({
    required String login,
    required String password,
  }) async {
    try {
      final isAuthValid =
          await _datasource.checkUser(login: login, password: password);
      return Right(isAuthValid);
    } catch (error) {
      return Left(Failure("[USERS REPO] Can not valid auth: $error"));
    }
  }
}
