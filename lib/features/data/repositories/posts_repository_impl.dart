import 'package:dartz/dartz.dart';
import 'package:logger/logger.dart';
import 'package:test_work/core/error/failure.dart';
import 'package:test_work/features/data/datasources/datasource.dart';
import 'package:test_work/features/domain/entities/comment.dart';
import 'package:test_work/features/domain/entities/post.dart';
import 'package:test_work/features/domain/repositories/posts_repository.dart';

class PostsRepositoryImpl implements PostsRepository {
  final Datasource _datasource;

  PostsRepositoryImpl(this._datasource);

  @override
  Future<Either<Failure, List<Post>>> getPosts() async {
    try {
      final List<Post> facts = await _datasource.getPosts();
      return Right(facts);
    } catch (error) {
      final String message = "[FACT REPO] Can not get facts: $error";
      Logger().e(message);
      return Left(Failure(message));
    }
  }

  @override
  Future<Either<Failure, List<Comment>>> getComments(int postId) async {
    try {
      final List<Comment> comments = await _datasource.getComments(postId);
      return Right(comments);
    } catch (error) {
      final String message = "[FACT REPO] Can not get comments: $error";
      Logger().e(message);
      return Left(Failure(message));
    }
  }
}
