import 'package:test_work/features/domain/entities/comment.dart';
import 'package:test_work/features/domain/entities/post.dart';

abstract class Datasource {
  Future<List<Post>> getPosts();

  Future<List<Comment>> getComments(int postId);

  Future<bool> checkUser({
    required String login,
    required String password,
  });
}
