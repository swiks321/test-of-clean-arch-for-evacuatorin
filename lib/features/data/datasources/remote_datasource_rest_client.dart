import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:test_work/features/data/models/comment_model.dart';
import 'package:test_work/features/data/models/post_model.dart';

part 'remote_datasource_rest_client.g.dart';

@RestApi(baseUrl: "https://jsonplaceholder.typicode.com/")
abstract class RemoteDatasourceRestClient {
  factory RemoteDatasourceRestClient(Dio dio) = _RemoteDatasourceRestClient;

  @GET('/posts')
  Future<List<PostModel>> getPosts();

  @GET('/posts/{postId}/comments')
  Future<List<CommentModel>> getComments(@Path("postId") int postId);
}