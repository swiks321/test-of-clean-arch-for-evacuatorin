import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:test_work/features/data/datasources/datasource.dart';
import 'package:test_work/features/data/datasources/remote_datasource_rest_client.dart';
import 'package:test_work/features/data/models/user_model.dart';
import 'package:test_work/features/domain/entities/comment.dart';
import 'package:test_work/features/domain/entities/post.dart';


class RemoteDatasourceImpl implements Datasource {
  final RemoteDatasourceRestClient restClient = RemoteDatasourceRestClient(Dio());

  @override
  Future<List<Post>> getPosts() async {
    return restClient.getPosts();
  }

  @override
  Future<bool> checkUser(
      {required String login, required String password}) async {
    return await Future.delayed(const Duration(seconds: 2), () {
      final bool isValid = UserModel(
            login: '123',
            passwordHash: sha256.convert(password.codeUnits),
          ) ==
          UserModel.defaultUser();
      return isValid;
    });
  }

  @override
  Future<List<Comment>> getComments(int postId) async {
    return restClient.getComments(postId);
  }
}
