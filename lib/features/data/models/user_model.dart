import 'package:crypto/crypto.dart';
import 'package:test_work/features/domain/entities/user.dart';

class UserModel extends User {
  UserModel({
    required String login,
    required Digest passwordHash,
  }) : super(
          login: login,
          passwordHash: passwordHash,
        );

  factory UserModel.defaultUser() => UserModel(login: '123', passwordHash: sha256.convert('123'.codeUnits));
}
