import 'package:json_annotation/json_annotation.dart';
import 'package:test_work/features/domain/entities/post.dart';

part 'post_model.g.dart';

@JsonSerializable()
class PostModel extends Post {
  PostModel({
    required int id,
    required String title,
    required String body,
  }) : super(
          id: id,
          title: title,
          body: body,
        );

  factory PostModel.fromJson(Map<String, dynamic> json) => _$PostModelFromJson(json);
}
