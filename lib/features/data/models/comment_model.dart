import 'package:json_annotation/json_annotation.dart';
import 'package:test_work/features/domain/entities/comment.dart';

part 'comment_model.g.dart';

@JsonSerializable()
class CommentModel extends Comment {
  CommentModel({
    required int id,
    required int postId,
    required String name,
    required String body,
  }) : super(
          id: id,
          postId: postId,
          name: name,
          body: body,
        );

  factory CommentModel.fromJson(Map<String, dynamic> json) => _$CommentModelFromJson(json);
}
