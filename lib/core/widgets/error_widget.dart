import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_work/core/error/failure.dart';

class ErrorWidget extends StatelessWidget {
  final Failure _failure;

  const ErrorWidget(this._failure, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        _failure.message,
        style: context.theme.textTheme.headline3!.copyWith(
          color: Colors.red.shade800,
        ),
      ),
    );
  }
}
