import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SecondaryButtonWidget extends StatelessWidget {
  const SecondaryButtonWidget(
      {Key? key, required this.text, required this.onPressed})
      : super(key: key);

  final String text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 20),
        textStyle: context.theme.textTheme.bodyText1,
      ),
      child: Text(
        text,
      ),
    );
  }
}
