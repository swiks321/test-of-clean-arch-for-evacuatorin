// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'injector.dart';

// **************************************************************************
// KiwiInjectorGenerator
// **************************************************************************

class _$Injector extends Injector {
  @override
  void _configure() {
    final KiwiContainer container = KiwiContainer();
    container.registerFactory<Datasource>((c) => RemoteDatasourceImpl());
    container.registerFactory<PostsRepository>(
        (c) => PostsRepositoryImpl(c<Datasource>()));
    container.registerFactory<UsersRepository>(
        (c) => UsersRepositoryImpl(c<Datasource>()));
    container.registerFactory((c) => Login(c<UsersRepository>()));
    container.registerFactory((c) => GetPosts(c<PostsRepository>()));
    container.registerFactory((c) => GetComments(c<PostsRepository>()));
    container.registerFactory((c) => LoginCubit(c<Login>()));
    container.registerFactory((c) => HomeCubit(c<GetPosts>()));
    container.registerFactory((c) => InfoCubit(c<GetComments>()));
  }
}
