import 'package:kiwi/kiwi.dart';
import 'package:test_work/features/data/datasources/datasource.dart';
import 'package:test_work/features/data/datasources/remote_datasource_impl.dart';
import 'package:test_work/features/data/repositories/posts_repository_impl.dart';
import 'package:test_work/features/data/repositories/users_repository_impl.dart';
import 'package:test_work/features/domain/repositories/posts_repository.dart';
import 'package:test_work/features/domain/repositories/users_repository.dart';
import 'package:test_work/features/domain/usecases/get_comments.dart';
import 'package:test_work/features/domain/usecases/get_posts.dart';
import 'package:test_work/features/domain/usecases/login.dart';
import 'package:test_work/features/presentation/home/controllers/home_cubit.dart';
import 'package:test_work/features/presentation/info/controllers/info_cubit.dart';
import 'package:test_work/features/presentation/login/controllers/login_cubit.dart';

part 'injector.g.dart';

abstract class Injector {
  static KiwiContainer? kiwiContainer;

  static void setup() {
    kiwiContainer = KiwiContainer();
    _$Injector()._configure();
  }

  static final resolve = kiwiContainer!.resolve;
  
  // Datasources
  @Register.factory(Datasource, from: RemoteDatasourceImpl)
  // Repos
  @Register.factory(PostsRepository, from: PostsRepositoryImpl)
  @Register.factory(UsersRepository, from: UsersRepositoryImpl)
  // Usecases
  @Register.factory(Login)
  @Register.factory(GetPosts)
  @Register.factory(GetComments)
  // Blocs/Cubits
  @Register.factory(LoginCubit)
  @Register.factory(HomeCubit)
  @Register.factory(InfoCubit)
  void _configure();
}
