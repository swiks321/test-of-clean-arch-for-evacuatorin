import 'package:flutter/material.dart';
import 'package:test_work/core/loc/localization.dart';

class TextFieldValidators {
  static FormFieldValidator<String> notEmptyValidator = (String? value) {
    if (value == null || value.isEmpty) {
      return Locals.lastLocale.requiredField;
    }
    return null;
  };
}
