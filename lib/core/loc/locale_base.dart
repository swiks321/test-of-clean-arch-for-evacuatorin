import 'package:flutter/material.dart';

@immutable
abstract class LocaleBase {
  String get more;
  String get comments;

  String get login;
  String get signIn;
  String get password;
  String get requiredField;
  String get invalidData;
  String get userNotFound;
}
