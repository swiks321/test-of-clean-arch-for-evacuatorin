import 'package:meta/meta.dart';
import 'package:test_work/core/loc/locale_base.dart';

@immutable
class RuLoc implements LocaleBase {
  @override
  String get comments => 'Комментарии';

  @override
  String get invalidData => 'Неверные данные';

  @override
  String get login => 'Логин';

  @override
  String get more => 'Больше';

  @override
  String get password => 'Пароль';

  @override
  String get signIn => 'Войти';

  @override
  String get requiredField => 'Обязательное поле';

  @override
  String get userNotFound => 'Пользователь с такими данными не найден';
}
