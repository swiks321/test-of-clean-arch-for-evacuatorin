import 'package:get/get.dart';
import 'package:test_work/core/routes/pages.dart';
import 'package:test_work/features/presentation/home/view/home_view.dart';
import 'package:test_work/features/presentation/login/view/login_view.dart';

class Routes {
  static const initialPage = Pages.login;

  static List<GetPage> routes = [
    GetPage(name: Pages.login, page: () => LoginView()),
    GetPage(name: Pages.home, page: () => HomeView()),
  ];
}
