import 'dart:ui';

import 'package:flutter/material.dart';

part 'values.dart';

class Themes {
  static ThemeData get lightTheme => ThemeData(
        brightness: Brightness.light,
        scaffoldBackgroundColor: _Values.white,
        primaryColor: _Values.violet,
        primaryColorDark: _Values.darkViolet,
        primaryColorLight: _Values.lightViolet,
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: _Values.violet,
          selectionColor: _Values.lightViolet,
          selectionHandleColor: _Values.lightViolet,
        ),
        buttonColor: _Values.violet,
        colorScheme: ColorScheme(
          onError: _Values.white,
          secondaryVariant: _Values.lightViolet,
          error: _Values.red,
          primaryVariant: _Values.lightViolet,
          primary: _Values.violet,
          onPrimary: _Values.white,
          secondary: _Values.darkViolet,
          onSecondary: _Values.white,
          brightness: Brightness.light,
          surface: _Values.lightViolet,
          background: _Values.white,
          onBackground: _Values.black,
          onSurface: _Values.black,
        ),
        textTheme: Typography.material2018(platform: TargetPlatform.iOS).black,
      );

  static ThemeData get darkTheme => lightTheme;
}
