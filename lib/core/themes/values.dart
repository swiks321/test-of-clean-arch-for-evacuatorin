part of 'themes.dart';

class _Values {
  static const Color black = Color(0xFF0C121E);
  static const Color white = Color(0xFFFFFFFF);
  static const Color violet = Color(0xFF815AC0);
  static const Color darkViolet = Color(0xFF6247AA);
  static const Color lightViolet = Color(0xFFDAC3E8);
  static const Color red = Colors.red;
}
