import 'dart:async';
import 'dart:developer' as dev;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_work/core/di/injector.dart';
import 'package:test_work/core/routes/routes.dart';
import 'package:test_work/core/themes/themes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Injector.setup();

  runZonedGuarded(
    () => runApp(MyApp()),
    (Object object, StackTrace stackTrace) => dev.log(
      object.toString(),
      error: object,
      stackTrace: stackTrace,
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: Themes.lightTheme,
      title: 'Test',
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.initialPage,
      getPages: Routes.routes,
    );
  }
}
